import React from 'react'
import RoomsFilter from './RoomFilter'
import RoomList from './RoomList'
import {withRoomConsumer} from '../context'
import Loading from "./Loading"
function RoomContainer({context}) {
    const {loading,sortedRooms,rooms} = context
    if(loading) {
        return <Loading/>
    }
    return(
        <>
     
        <RoomsFilter rooms = {rooms}/>
        <RoomList rooms = {sortedRooms}/>
        </>
    )
}





export default withRoomConsumer(RoomContainer)
/*export default function RoomContainer() {
    return (
        <RoomConsumer>        
        {valeu => {
            const {loading,sortedRooms,rooms} = valeu
            if(loading) {
                return <Loading/>
            }
            return(
        <div>
            Hello From Rooms Container
            <RoomsFilter rooms = {rooms}/>
            <RoomList rooms = {sortedRooms}/>
        </div>
        )
    }
    }
        </RoomConsumer>
    )
}
*/